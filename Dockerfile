FROM maven:alpine
WORKDIR /srv/
COPY src src
COPY pom.xml .
RUN mvn package

FROM openjdk:8-jdk-alpine 
WORKDIR /srv/
COPY --from=0 /srv/target/cicd-0.0.1.jar .
ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar cicd-0.0.1.jar" ]