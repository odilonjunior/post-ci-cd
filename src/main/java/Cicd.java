import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@RestController
@EnableAutoConfiguration
public class Cicd {

	@RequestMapping("/")
	String home() {
		return "SpringBoot do CI/CD";
	}

	public static void main(String[] args) {
		SpringApplication.run(Cicd.class, args);
	}

}
